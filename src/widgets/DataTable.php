<?php

namespace ffsoft\widgets;

use ffsoft\assets\DataTableAsset;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Json;

/**
 * Class DataTable
 *
 * @package ffsoft\widgets
 */
class DataTable extends Widget
{
    /** @var string Table ID */
    public $id;
    /**
     * @var array the HTML attributes for the datatables table element.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $tableOptions = ["class" => "table table-striped- table-bordered table-hover table-checkable"];
    /**
     * @var array the HTML attributes for the datatables table element.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    /**
     * @var array
     */
    public $data = [
        "type"            => 'remote',
        "source"          => [
            "read" => [
                "url"    => '/main/data',
                "method" => 'GET',
                // additional headers
                // additional request params
                "params" => [],
            ],
        ],
        "pageSize"        => 10,
        "saveState"       => [
            "cookie"     => true,
            "webstorage" => true,
        ],
        "serverPaging"    => true,
        "serverFiltering" => true,
        "serverSorting"   => true,
        //"autoColumns"     => false,
    ];
    /**
     * @var bool
     */
    public $sortable = true;
    /**
     * @var bool
     */
    public $pagination = true;
    /**
     * @var array
     */
    public $layout = [
        "theme"  => 'default',
        "class"  => 'm-datatable--brand',
        "scroll" => false,
        "height" => null,
        "footer" => false,
        "header" => true,

        "smoothScroll" => [
            "scrollbarShown" => true,
        ],

        "spinner" => [
            "overlayColor" => '#000000',
            "opacity"      => 0,
            "type"         => 'loader',
            "state"        => 'brand',
            "message"      => true,
        ],

        "icons" => [
            "sort"       => ["asc" => 'la la-arrow-up', "desc" => 'la la-arrow-down'],
            "pagination" => [
                "next"  => 'la la-angle-right',
                "prev"  => 'la la-angle-left',
                "first" => 'la la-angle-double-left',
                "last"  => 'la la-angle-double-right',
                "more"  => 'la la-ellipsis-h',
            ],
            "rowDetail"  => ["expand" => 'fa fa-caret-down', "collapse" => 'fa fa-caret-right'],
        ],
    ];
    /**
     * @var array
     */
    public $search = [
        // enable trigger search by keyup enter
        "onEnter" => false,
        // input text for search
        //"input"   => "$('#generalSearch')",
        // search delay in milliseconds
        "delay"   => 400,
    ];
    /**
     * @var array
     */
    public $detail = [
        "title"   => 'Load sub table',
        "content" => "function() {}",
    ];
    /**
     * @var array
     */
    public $rows = [
        "callback" => "function() {}",
        // auto hide columns, if rows overflow. work on non locked columns
        "autoHide" => false,
    ];
    /**
     * @var
     */
    public $columns;
    /**
     * @var array
     */
    public $toolbar = [
        "layout"    => ['pagination', 'info'],
        "placement" => ['bottom'],  //'top', 'bottom'
        "items"     => [
            "pagination" => [
                "type"           => 'default',
                "pages"          => [
                    "desktop" => [
                        "layout"      => 'default',
                        "pagesNumber" => 6,
                    ],
                    "tablet"  => [
                        "layout"      => 'default',
                        "pagesNumber" => 3,
                    ],
                    "mobile"  => [
                        "layout" => 'compact',
                    ],
                ],
                "navigation"     => [
                    "prev"  => true,
                    "next"  => true,
                    "first" => true,
                    "last"  => true,
                ],
                "pageSizeSelect" => [10, 20, 30, 50, 100],
            ],
            "info"       => true,
        ],
    ];
    /**
     * @var array
     */
    public $translate = [
        "records" => [
            "processing" => 'Please wait...',
            "noRecords"  => 'No records found',
        ],
        "toolbar" => [
            "pagination" => [
                "items" => [
                    "default" => [
                        "first"  => 'First',
                        "prev"   => 'Previous',
                        "next"   => 'Next',
                        "last"   => 'Last',
                        "more"   => 'More pages',
                        "input"  => 'Page number',
                        "select" => 'Select page size',
                    ],
                    "info"    => 'Displaying {{start}} - {{end}} of {{total}} records',
                ],
            ],
        ],
    ];

    /**
     *
     */
    public function init()
    {
        parent::init();
        DataTableAsset::register($this->getView());

        $this->options = ArrayHelper::merge($this->options, [
            'data'       => $this->data,
            'sortable'   => $this->sortable,
            'pagination' => $this->pagination,
            'layout'     => $this->layout,
            'search'     => $this->search,
            'detail'     => $this->detail,
            'rows'       => $this->rows,
            'columns'    => $this->columns,
            'toolbar'    => $this->toolbar,
            'translate'  => $this->translate,
        ]);
        //$this->initColumns();
    }

    /**
     *
     */
    public function run()
    {
        $id = isset($this->id) ? $this->id : $this->getId();

        echo Html::beginTag('table', ArrayHelper::merge(['id' => $id], $this->tableOptions));
        echo Html::endTag('table');

        $this->getView()->registerJs('jQuery("#' . $id . '").mDatatable(' . $this->getOptions() . ');');
    }

    /**
     * Get JSON array of Client options to be passed to Datatables plugin
     *
     * @return string
     */
    protected function getOptions()
    {
        return Json::encode($this->options);
    }

    /**
     *
     */
    protected function initColumns()
    {
        if (isset($this->options['columns'])) {
            foreach ($this->options['columns'] as $key => $value) {
                if (is_string($value)) {
                    $this->options['columns'][$key] = ['data' => $value, 'title' => Inflector::camel2words($value)];
                }
                if (isset($value['type'])) {
                    if ($value['type'] == 'link') {
                        //$value['class'] = LinkColumn::className();
                    }
                }
                if (isset($value['class'])) {
                    $column = \Yii::createObject($value);
                    $this->options['columns'][$key] = $column;
                }
            }
        }
    }
}