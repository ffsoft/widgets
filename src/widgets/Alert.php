<?php

namespace ffsoft\widgets;

use yii\bootstrap\Widget;

class Alert extends Widget
{
    public $view;

    public function run()
    {
        return $this->render('alerts/view', [
            'view' => $this->view,
        ]);
    }
}