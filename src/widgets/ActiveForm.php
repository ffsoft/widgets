<?php

namespace ffsoft\widgets;

use yii\widgets\ActiveForm as YiiActiveForm;

class ActiveForm extends YiiActiveForm
{
    /**
     * @var array|\Closure the default configuration used by [[field()]] when creating a new field object.
     * This can be either a configuration array or an anonymous function returning a configuration array.
     * If the latter, the signature should be as follows:
     *
     * ```php
     * function ($model, $attribute)
     * ```
     *
     * The value of this property will be merged recursively with the `$options` parameter passed to [[field()]].
     *
     * @see fieldClass
     */
    public $fieldConfig = [
        'options'      => [
            'class' => 'col-lg-3 m--margin-bottom-10-tablet-and-mobile',
        ],
        'errorOptions' => [
            'class' => 'm-form__help',
        ],
        'template'     => "{label}\n{input}\n{hint}\n{error}",
        'inputOptions' => [
            'class' => 'form-control m-input m-input--air',
        ],
        'labelOptions' => [
            'class'     => 'col-form-label',
            'tepmplate' => "<span>{label}</span>",
        ],
    ];
    /**
     * @var array the HTML attributes (name-value pairs) for the form tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [
        'class' => 'm-form m-form--fit m--margin-bottom-20',
    ];
}