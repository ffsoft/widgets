<?php

namespace ffsoft\widgets;

use yii\base\Widget;

/**
 * Class GA
 */
class GA extends Widget
{
    /**
     * The GA tracking ID.
     *
     * @var string
     */
    public $id = null;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $params = [
            'id' => $this->id,
        ];

        return $this->render('GA/view', $params);
    }
}
