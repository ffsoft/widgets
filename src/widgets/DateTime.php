<?php

namespace ffsoft\widgets;

use Yii;
use yii\bootstrap\Widget;

class DateTime extends Widget
{
    /**
     * @var $format    string
     * @var $timestamp int
     */
    public $format = 'd.m.y H:i';
    public $time = null;
    public $timestamp = null;
    public $timezone = null;

    public function run()
    {
        if ($this->timestamp === null) {
            $this->timestamp = time();
        }

        if ($this->timezone === null) {
            $identity = Yii::$app->getUser()->getIdentity();
            if ($identity !== null) {
                $tz = $identity->profile->getTimeZone();
            } else {
                $tz = new \DateTimeZone(Yii::$app->getTimeZone());
            }
            $this->timezone = $tz;
        }

        if ($this->time === null) {
            $this->time = new \DateTime;
            $this->time->setTimestamp($this->timestamp);
            $this->time->setTimezone($this->timezone);
        }

        return $this->render('DateTime/view', [
            'format' => $this->format,
            'time'   => $this->time,
        ]);
    }
}

