<?php

namespace ffsoft\widgets;

use yii\bootstrap\Widget;

class Modal extends Widget
{
    /** @var string */
    public $id = 'helpModal';

    /** @var int */
    public $timeout = 10000;

    /** @var string */
    public $linkSelector = '.action-modal';

    /** @var bool */
    public $enablePushState = false;

    public function run()
    {
        $params = [
            'id'              => $this->id,
            'timeout'         => $this->timeout,
            'linkSelector'    => $this->linkSelector,
            'enablePushState' => $this->enablePushState
        ];

        return $this->render('modal/view', $params);
    }
}