<?php

use yii\widgets\Pjax;

/**
 * @var $id                 string
 * @var $timeout            int
 * @var $linkSelector       string
 * @var $enablePushState    bool
 */

/** @noinspection PhpUnhandledExceptionInspection */
Pjax::widget([
    'enablePushState' => $enablePushState,
    'id'              => $id,
    'linkSelector'    => $linkSelector,
    'timeout'         => $timeout,
]);

?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalContent" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="helpModal"></div>
        </div>
    </div>
<?php
$this->registerJs("
 $('document').ready(function(){
    $('#helpModal').on('pjax:end', function() {
        $('#modalContent').modal({backdrop: false});
    });
 });
"); ?>