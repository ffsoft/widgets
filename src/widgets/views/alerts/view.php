<?php
/**
 * @var $view string
 */
foreach (Yii::$app->session->getAllFlashes() as $key => $value) {
    if (is_array($value)) {
        if ($value['view'] == $view) {

            echo '<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30 alert-' . $value['type'] . '" role="alert">
                    <div class="m-alert__icon">
                        <i class="flaticon-exclamation m--font-brand"></i>
                    </div>
                    <div class="m-alert__text">
                        ' . $value['message'] . '
                    </div>
                </div>';
        }
    }
}


